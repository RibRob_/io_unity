﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bob : MonoBehaviour
{
    [SerializeField]
    private float amp, spd;

    private float initY;

    // Start is called before the first frame update
    void Start()
    {
        initY = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        pos.y = initY + amp * Mathf.Sin(spd * Time.time);
        transform.position = pos;
    }
}
