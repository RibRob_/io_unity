﻿//Rob Harwood and Meg'n
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//Parent class to othe block scripts. The code here are universal functions.

public class Block : MonoBehaviour {

    //Private Members
    private int _particlePoolIndex = 0;

    //Protected Members
    protected bool pShot = true;
    protected bool pGrow = true;
    protected Rigidbody rb;

    //Public Members
    public int damage = 5;
    public float blastRadius = 4f;
    public AudioClip explosionSFX;
    public UnityEvent ueDeactivated;
    public GameObject destroyParticles;
    public GameObject explosionParticles;
    public GameObject[] particlePool = new GameObject[10];

    // Start is called before the first frame update
    //void Start() {
        
    //    if (rb = null) {
    //        Debug.Log("rb is null");
    //    }
    //    else {
    //        Debug.Log("rb isn't null");
    //    }

    //    //Add as listener to deconstruct event

    //}

    //private void Update() {

    //}

    /// <summary>
    /// Transforms a shot block into a platforming block
    /// </summary>
    /// <param name="contactPoint">First point of contact between the block and the surface it's adhering to</param>
    private void _Grow(Collision collision) {
        //Get rigid body
        rb = GetComponent<Rigidbody>();

        //Rescale the block
        transform.localScale = new Vector3(2.01f, 1.01f, 2.01f);

        //Point where the block first contacts something else
        Vector3 contactPoint = collision.GetContact(0).point;

        //Get values for the grid square the block landed on
        float xGrid = Mathf.Round(contactPoint.x);
        float yGrid = Mathf.Round(contactPoint.y);
        float zGrid = Mathf.Round(contactPoint.z);

        //Set position and rotation to snap to the grid and lock it in place
        transform.position = new Vector3(xGrid + Mathf.Round(transform.localScale.x) / 2, yGrid + Mathf.Round(transform.localScale.y) / 2, zGrid + Mathf.Round(transform.localScale.z) / 2);
        transform.rotation = Quaternion.identity;
        if (collision.collider.CompareTag("Moving Platform"))
            transform.SetParent(collision.transform);
        else
            rb.constraints = RigidbodyConstraints.FreezeAll;
        rb.isKinematic = true;
    }

    public void Shoot(){
        pShot = true;
    }


    public virtual void Deconstruct() {
        Debug.Log("Deconstructed...");
        //Play destruction sequence
        Destroy(this.gameObject);
    }

    /// <summary>
    /// Places the particles for breaking blocks using Object Pooling.
    /// </summary>
    /// <param name="collision">Collision.</param>
    private void _PlaceParticle(Collision collision) {
        particlePool[_particlePoolIndex].transform.position = collision.transform.position;
        particlePool[_particlePoolIndex].SetActive(true);
        particlePool[_particlePoolIndex].SendMessage("ResetTimer");
        _particlePoolIndex++;
    }

    protected virtual void OnCollisionEnter(Collision collision){

        ElectricBock electric = collision.gameObject.GetComponent<ElectricBock>();
        MagneticBlock magnetic = collision.gameObject.GetComponent<MagneticBlock>();

        //Had to rewrite this cause it was giving me problems earlier.
        //Looks MUCH cleaner.
        if (collision.collider.CompareTag("Enemy") && pShot) {

            GameObject DestroyParticle = Instantiate(destroyParticles, this.transform.position, this.transform.rotation);
            this.gameObject.SetActive(false);
            collision.gameObject.SendMessage("TakeDamage", damage);
            pShot = false;
        }
        else if (magnetic != null) {
            ElectricBock eb = this.GetComponent<ElectricBock>();
            if (eb != null) {
                Explode(collision);
            }
        }
        else if (electric != null) {
            MagneticBlock mb = this.GetComponent<MagneticBlock>();
            if (mb != null) {
                Explode(collision);
            }
        }
        else {
            pShot = false;

            //Normal of that point, to check if it's the top of something
            Vector3 pointNormal = collision.GetContact(0).normal;

            //Criteria for growth:
                //Isn't already grown
                //Point of contact is the top of a surface
                //Contacted surface is walkable terrain, moving platform, OR another block
            if (pGrow && IsTop(pointNormal) && (collision.collider.CompareTag("Walkable Terrain") || collision.collider.CompareTag("Moving Platform") || collision.collider.CompareTag("Block")))
            {
                _Grow(collision);
                pGrow = false;
            }
        }
    }

    private void Explode(Collision collision) {
        Collider[] colliders = Physics.OverlapSphere(transform.position, blastRadius);
        List<Enemy> enemiesHit = new List<Enemy>();

        foreach (Collider c in colliders) {
            Enemy newEnemy = c.GetComponent<Enemy>();

            if (newEnemy != null) {
                enemiesHit.Add(newEnemy);
            }
        }

        foreach (Enemy e in enemiesHit) {
            e.TakeDamage(damage);
        }

        GameObject expParticles = Instantiate(explosionParticles, collision.transform.position, collision.transform.rotation);
        this.gameObject.SetActive(false);
        collision.gameObject.SetActive(false);
        pShot = false;


    }

    /// <summary>
    /// Checks to see if point is on the top of a surface (normal is ~vertical)
    /// </summary>
    /// <param name="contact">Point of contact</param>
    /// <returns></returns>
    private bool IsTop (Vector3 contact)
    {
        if (contact.y > 0.9)
            return true;
        else
            return false;
    }


}



