﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MagneticBlock : Block { //Honestly just ignore this whole thing. Don't put it on anything. Something's broken.
                                        // - Meg'n
    //Public Members

    //Private Members
    //private PoolObject[] _particlePool;

    //Protected Members

    //protected override void OnCollisionEnter(Collision collision) {
    //    if (collision.collider.CompareTag("Enemy") && pShot) {

    //        GameObject DestroyParticle = Instantiate(destroyParticles, this.transform.position, this.transform.rotation);
    //        this.gameObject.SetActive(false);
    //        collision.gameObject.SendMessage("TakeDamage", damage);
    //        pShot = false;
    //    }
    //    else if (collision.collider.CompareTag("Power Block") && pShot) {
    //        //Explosion
    //        Debug.Log("BOOM!");
    //    }
    //    else {
    //        pShot = false;
    //        pGrow = true;
    //    }
    //}

    [SerializeField]
    private float pullRadius, stopDistance, speed;

    [SerializeField]
    private GameObject player;

    private List<GameObject> enemies;

    private void Start()
    {
        enemies = new List<GameObject>();
        StartCoroutine(CheckEnemies());
    }

    private void Update()
    {
        
    }

    private void Pull()
    {
        if (!pGrow)
        {
            foreach (GameObject en in enemies)
            {
                en.GetComponent<NavMeshAgent>().isStopped = true;
                Vector3 direction = Vector3.zero;
                if (Vector3.Distance(transform.position, en.transform.position) > stopDistance)
                {
                    direction = en.transform.position - transform.position;
                    en.GetComponent<Rigidbody>().AddRelativeForce(direction.normalized * speed, ForceMode.Force);
                }
            }
        }
    }

    private IEnumerator CheckEnemies()
    {
        while (!pGrow)
        {
            enemies.Clear();
            GameObject[] en = GameObject.FindGameObjectsWithTag("Enemy");
            for (int i = 0; i < en.Length; i++)
            {
                float dist = Vector3.Distance(transform.position, en[i].transform.position);
                if (dist <= pullRadius)
                {
                    enemies.Add(en[i]);
                    Debug.Log("Enemy added: " + en[i].name);
                }
            }
            yield return new WaitForSeconds(1.0f);
        }
        yield return new WaitForSeconds(1.0f);
    }

    public override void Deconstruct()
    {
        Debug.Log("Deconstructed...");
        //Play destruction sequence
        foreach (GameObject en in enemies)
        {
            en.GetComponent<NavMeshAgent>().isStopped = false;
        }
        Destroy(this.gameObject);
    }
}
