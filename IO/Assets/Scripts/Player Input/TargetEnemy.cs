﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TargetEnemy : PlayerInput {

    //Public Members
    public EnemyManager enemyManager;
    public string targetButton = "";
    public Fade targetSystemFade;
    public Transform playerCamTrans;
    public Camera playerCam;
    //public UnityEvent targeting;
    //public UnityEvent notTargeting;
    public Transform centerOfScreen;
    public GameObject targetUI;
    public PlayerMovement playerMovement;
    public PlayerBlockCreator blockCreator;

    //Private Members
    private List<GameObject> _enemyGOs = new List<GameObject>();
    private bool _targeting = false;
    private GameObject _enemyGOToTarget;
    private Vector3 _lastRot;

    //Protected Members

    // Start is called before the first frame update
    void Start() {
        pDetectOS();
        ChangeToKeyboardSupport();
    }

    private void FixedUpdate() {
        _GetTarget();
        Target();
        Untarget();
    }

    private void _GetTarget() {
        //Debug.Log("Target Button Value: " + (Input.GetAxis(targetButton) > 0));
        if (Input.GetButtonDown(targetButton) || Input.GetAxis(targetButton) > 0 && !_targeting) {
            _targeting = true;

            _GetEnemies();
            _enemyGOToTarget = _FindClosest();

        }
    }

    private void _GetEnemies() {
        _enemyGOs.Clear();

        foreach (Enemy e in enemyManager.enemies) {
            Renderer rend = e.gameObject.GetComponent<Renderer>();

            if (rend != null) {
                if (rend.isVisible && e.currentHP > 0) {
                    _enemyGOs.Add(e.gameObject);
                }
            }
            else {
            }

        }

    }

    private GameObject _FindClosest() {
        GameObject closestEnemy = null;
        Vector3 center = centerOfScreen.position;
        Vector3 enemyPosition;
        float smallestDistance = 7000000f;

        foreach (GameObject go in _enemyGOs) {
            enemyPosition = playerCam.WorldToScreenPoint(go.transform.position);

            //Calculate distance from center
            float distance = Vector3.Distance(center, enemyPosition);

            //Ray cast to make sure nothing is in front of it.
            RaycastHit hit;
            Physics.Raycast(playerCamTrans.position, go.transform.position, out hit);

            if (distance < smallestDistance) {
                closestEnemy = go;
                smallestDistance = distance;
            }
        }

        return closestEnemy;
    }

    public void Target() {
        if (Input.GetButton(targetButton) || (Input.GetAxis(targetButton) > 0 && _targeting)) {
            if (_enemyGOToTarget != null) {
                playerMovement.lookEnabled = false;
                playerCamTrans.LookAt(_enemyGOToTarget.transform);
                _lastRot = playerCamTrans.transform.rotation.eulerAngles;

                if (targetSystemFade.fadeOut) {
                    targetSystemFade.fadeIn = true;
                    targetSystemFade.fadeOut = false;
                }
            }
            else {
                Debug.Log("Cannot target");
            }
        }
    }

    public void Untarget() {
        if (Input.GetButtonUp(targetButton) || (Input.GetAxis(targetButton) <= 0 && _targeting)) {
            _targeting = false;
            playerMovement.lookEnabled = true;
            targetSystemFade.fadeIn = false;
            targetSystemFade.fadeOut = true;
            playerCamTrans.eulerAngles = _lastRot;
        }
    }

    public override void ChangeToControllerSupport() {
        Debug.Log("Finding controller support...");
        pcontrollerPresent = true;

        if (pOS == "Window") {
            targetButton = "LeftBumper_XBx_Windows";
        }
        else if (pOS == "Mac") {
            targetButton = "LeftBumper_XBx_Mac";
        }
        else if (pOS == "Linux") {
            targetButton = "LeftBumper_XBx_Linux";
        }
        else {
            Debug.Log("No controller support for this system.");
            ChangeToKeyboardSupport();
        }
    }


    public override void ChangeToKeyboardSupport() {
        pcontrollerPresent = false;
        targetButton = "Left Shift";
    }
}
