﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour{

    //Private Members

    //Protected Members
    protected string pOS = "Other";
    protected bool pcontrollerPresent = false;
    protected CustomFPSCamera pcustomFPSCamera;

    //Public Members


    // Start is called before the first frame update
    void Start() {
        pDetectOS();
        ChangeToKeyboardSupport();
    }

    protected virtual void pTrackTimers() { 
    }

    public virtual void ChangeToKeyboardSupport() { 
    
    }

    public virtual void ChangeToControllerSupport(){

    }

    /// <summary>
    /// Detects the OS the player is using.
    /// </summary>
    protected void pDetectOS(){
        if (SystemInfo.operatingSystemFamily == OperatingSystemFamily.MacOSX){
            pOS = "Mac";
        }
        else if (SystemInfo.operatingSystemFamily == OperatingSystemFamily.Linux){
            pOS = "Linux";
        }
        else if (SystemInfo.operatingSystemFamily == OperatingSystemFamily.Windows){
            pOS = "Windows";
        }
        else{
            pOS = "Other";
        }
        Debug.Log("OS detected: " + pOS);
    }
}
