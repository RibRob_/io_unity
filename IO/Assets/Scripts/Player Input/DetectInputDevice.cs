﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//Unity has no way of dynamically checking wether or not controllers have been
//plugged or unplugged. This script takes care of that issue and sends events
//based on changes.

public class DetectInputDevice : MonoBehaviour{

    //Private Members
    private float _waitSec = 2.0f;//Amount of time before next check
    private int _numControllersPrev = 0;//Number of controllers at last check
    private bool initialCheck = true;

    //Public Members
    public UnityEvent Unplugged;
    public UnityEvent Plugged;


    // Start is called before the first frame update
    void Start(){
        Debug.Log("Checking for controllers...");
        StartCoroutine(CheckControllers());
    }

    /// <summary>
    /// Uses recursion to check if a controller gets plugged or unplugged every _waitSec.
    /// </summary>
    /// <returns>The controllers.</returns>
    IEnumerator CheckControllers() {

        yield return new WaitForSecondsRealtime(_waitSec);
        Debug.Log("Checking for controllers...");
        string[] joysticks = Input.GetJoystickNames();

        if (joysticks.Length != _numControllersPrev) {//If there was a change in the number of joysticks since last checking
            if(joysticks.Length == 0) { //If there are no joysticks
                Debug.Log("Controller got unplugged.");
                _numControllersPrev = joysticks.Length;
                Unplugged.Invoke();//Call unplugged event
            }
            else if (_numControllersPrev == 0 && joysticks.Length >= 1) { //If joysticks recently got plugged in.
                Debug.Log("Controller got plugged.");
                _numControllersPrev = joysticks.Length;
                Plugged.Invoke();//Call plugged event
            }
            else { //More than one controller was plugged or whatever, so no change is necessary
                _numControllersPrev = joysticks.Length;
            }
        }

        StopAllCoroutines();
        StartCoroutine(CheckControllers());
    }
}