﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayOnPlatform : MonoBehaviour
{
    Transform currentPlatform;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Moving Platform")
        {
            currentPlatform = collision.gameObject.transform;
            transform.SetParent(currentPlatform);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Moving Platform")
        {
            currentPlatform = null;
            transform.parent = null;
        }
    }
}
