﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingPoint : MonoBehaviour
{
    //This script handles player spawning and respawning

    //This is the point where the player starts the game
    [SerializeField]
    private Transform startPoint;

    //A list of spawn points from doorways
    [SerializeField]
    private List<Transform> entryPoints;

    private Transform lastPoint;

    //This records the last door the player went through
    private enum Spawn { start, l1_lower, l1_upperEast, l1_upperWest, l2_lower, l2_upper, l3_lower };
    private Spawn lastEntry;

    // Start is called before the first frame update
    void Start()
    {
        //Set the player's position and rotation to match the starting point
        transform.position = startPoint.position;
        transform.rotation = startPoint.rotation;

        //Sets the record of the last door/respawn point to the starting position
        lastEntry = Spawn.start;
        lastPoint = entryPoints[(int)lastEntry];
    }

    /// <summary>
    /// Set the respawn point of the player
    /// </summary>
    /// <param name="i">The index of the new spawn point in the list of points</param>
    public void SetSpawn(int i)
    {
        lastEntry = (Spawn)i;
        lastPoint = entryPoints[(int)lastEntry];
    }

    /// <summary>
    /// Sets the player's position and rotation to the current spawn point
    /// </summary>
    public void Respawn()
    {
        gameObject.transform.position = lastPoint.position;
        gameObject.transform.rotation = lastPoint.rotation;
    }
}
