﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    //Public Members
    public int currentHP = 100;
    public int maxHP = 100;
    public CameraShake cameraShake;

    //Private Members

    //Protected Members


    // Start is called before the first frame update
    void Start() {
        currentHP = 100;
    }

    // Update is called once per frame
    void Update() {
        _CheckHealth();
    }

    private void _CheckHealth() {
        if (currentHP <= 0) {
            SceneManager.LoadScene("MainMenu");
        }
    }

    public void TakeDamage(object obj) {
        int damage = int.Parse(obj.ToString());
        Debug.Log("I took " + damage + " damage!");
        currentHP -= damage;
        StartCoroutine(cameraShake.Shake(0.14f, 0.5f));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Health")
        {
            currentHP += 20;
            Destroy(other.gameObject);
        }
    }

}
