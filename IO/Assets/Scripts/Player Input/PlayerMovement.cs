﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//This script takes care of all input strings

public class PlayerMovement : PlayerInput {

    //Private Members
    private string _horizontalMovement = "";
    private string _verticalMovement = "";
    private string _horizontalLook = "";
    private string _verticalLook = "";
    private string _jump = "";
    private string _interact = "";
    private string _pickUp = "";
    private string _openHotWheel = "";
    private string _targetEnemy = "";
    private string _pause = "";
    private bool _isPaused = false;
    private float _defaultSpeed = 5f;
    [SerializeField]
    private float _currentSpeed = 5f;
    [SerializeField]
    private GameObject _fpsCameraGO;
    [SerializeField]
    private float _lookSensitivity = 5;
    private float _lookX = 0;
    private float _lookY = 0;
    private float _viewRange = 85;
    private int _maxNumJumps = 1;
    private int _currentNumJumps = 1;
    [SerializeField]
    private float _jumpForce = 10;
    private Vector3 _movement;
    [SerializeField]
    private Rigidbody _playerRB;
    [SerializeField]
    private float _jumpMomentum = 30;
    [SerializeField]
    private Transform _frontGorundCheckTransform;
    [SerializeField]
    private Transform _backGorundCheckTransform;

    //Public Members
    public bool movementEnabled = true;
    public bool lookEnabled = true;
    public bool pauseEnabled = true;
    public bool jumpEnabled = true;
    public bool trackingEnabled = true;
    public UnityEvent ueUnlockCursor;
    public UnityEvent ueLockCursor;

    //Accessors


    void FixedUpdate() {
        _Move();
        _Look();
        _Jump();
    }

    private void Update(){
        _Pause();
    }

    /// <summary>
    /// Player movement calculations.
    /// </summary>
    /// <param name="horizMove">Horiz move.</param>
    /// <param name="vertMove">Vert move.</param>
    /// <param name="horizLook">Horiz look.</param>
    /// <param name="vertLook">Vert look.</param>
    private void _Move() {
        if (movementEnabled) {

            float moveFB = Input.GetAxis(_verticalMovement) * _currentSpeed;
            float moveLR = Input.GetAxis(_horizontalMovement) * _currentSpeed;

            if (pcontrollerPresent) { //Calculations based on controller input.
                _movement = new Vector3(moveLR, 0, -moveFB);
            }
            else { //Calculations based on keyboard input
                _movement = new Vector3(moveLR, 0, moveFB);
            }

            this.transform.Translate(_movement * Time.deltaTime);
        }
    }

    /// <summary>
    /// Allows the player to look around the world.
    /// </summary>
    private void _Look() {
        if (lookEnabled) {

            _lookX += Input.GetAxis(_horizontalLook) * _lookSensitivity;
            _lookY += Input.GetAxis(_verticalLook) * _lookSensitivity;

            _lookY = Mathf.Clamp(_lookY, -_viewRange, _viewRange);//Restrict rotation based on viewrange

            Vector3 newRot = new Vector3(0, _lookX, 0);
            Vector3 newCamRot;

            //A controller's vertical input isn't inversed, but a mouse's is.
            if (pcontrollerPresent) { //New rotation based on controller input.
                newCamRot = new Vector3(_lookY, _lookX, 0);
            }
            else { //New rotation based on mouse input.
                newCamRot = new Vector3(-_lookY, _lookX, 0);
            }

            this.transform.eulerAngles = newRot;
            _fpsCameraGO.transform.eulerAngles = newCamRot;
        }
    }

    /// <summary>
    /// Momentum based jump.
    /// </summary>
    private void _Jump() {
        if (Input.GetButtonDown(_jump) && (jumpEnabled && _currentNumJumps > 0)) {

            Vector3 momentum;

            momentum = new Vector3(0, _jumpForce, -0);

            //if (pcontrollerPresent) { //Calculations based on controller input.
            //    momentum = new Vector3(0, _jumpForce, -0);
            //}
            //else { //Calculations based on keyboard input
            //    momentum = new Vector3(0, _jumpForce, 0);
            //}

            momentum = momentum * _jumpMomentum;
            _playerRB.AddRelativeForce(momentum);
            _currentNumJumps--;
        }
    }

    /// <summary>
    /// Checks wether or not the player is pausing.
    /// </summary>
    private void _Pause(){
        if (pauseEnabled) {
            if (Input.GetButtonDown(_pause) && !_isPaused) {//If the player presses the pause key while not paused
                Debug.Log("Pausing...");
                Unlock();
                _isPaused = true;
                ueUnlockCursor.Invoke();
                lookEnabled = false;
                movementEnabled = false;
            }
            else if (Input.GetButtonDown(_pause) && _isPaused) {//If the player presses the pause key while paused
                Debug.Log("Unpausing...");
                Lock();
                _isPaused = false;
                ueLockCursor.Invoke();
                lookEnabled = true;
                movementEnabled = true;
            }
        }
    }

    /// <summary>
    /// Locks the player.
    /// </summary>
    public void Lock() {
        if (!pcontrollerPresent) {
            Debug.Log("Locking cursor...");
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    /// <summary>
    /// Unlocks the player.
    /// </summary>
    public void Unlock() {
        if (!pcontrollerPresent) {
            Debug.Log("Unlocking cursor...");
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    /// <summary>
    /// Changes input strings to keyboard support.
    /// </summary>
    public override void ChangeToKeyboardSupport() {
        _horizontalMovement = "HorizontalMovement_KB";
        _verticalMovement = "VerticalMovement_KB";
        _horizontalLook = "Mouse X";
        _verticalLook = "Mouse Y";
        _jump = "Space";
        _interact = "Q";
        _pickUp = "Q";
        _openHotWheel = "Mouse ScrollWheel";
        _targetEnemy = "Left Shift";
        _pause = "Escape";
        pcontrollerPresent = false;
        Debug.Log("Keyboard support enabled.");
    }

    /// <summary>
    /// Chooses the correct set of strings for controller support based on the OS.
    /// </summary>
    public override void ChangeToControllerSupport() {
        Debug.Log("Finding controller support...");
        pcontrollerPresent = true;

        if (pOS == "Windows") {
             _horizontalMovement = "HorizontalMovement_XBx_Windows";
             _verticalMovement = "VerticalMovement_XBx_Windows";
             _horizontalLook = "HorizontalLook_XBx_Windows";
             _verticalLook = "VerticalLook_XBx_Windows";
             _jump = "A_XBx_Windows";
             _interact = "Y_XBx_Windows";
             _pickUp = "B_XBx_Windows";
             _openHotWheel = "LeftJoystickButton_XBx_Windows";
             _targetEnemy = "RightJoystickButton_XBx_Windows";
             _pause = "Pause_XBx_Windows";
            Debug.Log("Windows controller support enabled.");
        }
        else if (pOS == "Mac") {
            _horizontalMovement = "HorizontalMovement_XBx_Mac";
            _verticalMovement = "VerticalMovement_XBx_Mac";
            _horizontalLook = "HorizontalLook_XBx_Mac";
            _verticalLook = "VerticalLook_XBx_Mac";
            _jump = "A_XBx_Mac";
            _interact = "Y_XBx_Mac";
            _pickUp = "B_XBx_Mac";
            _openHotWheel = "LeftJoystickButton_XBx_Mac";
            _targetEnemy = "RightJoystickButton_XBx_Mac";
            _pause = "Pause_XBx_Mac";
            Debug.Log("Mac controller support enabled.");
        }
        else if (pOS == "Linux") {
            _horizontalMovement = "HorizontalMovement_XBx_Linux";
            _verticalMovement = "VerticalMovement_XBx_Linux";
            _horizontalLook = "HorizontalLook_XBx_Linux";
            _verticalLook = "VerticalLook_XBx_Linux";
            _jump = "A_XBx_Linux";
            _interact = "Y_XBx_Linux";
            _pickUp = "B_XBx_Linux";
            _openHotWheel = "LeftJoystickButton_XBx_Linux";
            _targetEnemy = "RightJoystickButton_XBx_Linux";
            _pause = "Pause_XBx_Linux";
            Debug.Log("Linux controller support enabled.");
        }
        else {
            Debug.Log("No controller support for this system.");
            ChangeToKeyboardSupport();
        }

    }

    private void OnCollisionEnter(Collision collision){
        //Jump stuff
        if(collision.gameObject.tag == "Walkable Terrain" || collision.gameObject.tag == "Block") {

            RaycastHit hitFront;
            Physics.Raycast(_frontGorundCheckTransform.position, Vector3.down, out hitFront, 1.1f);
            RaycastHit hitBack;
            Physics.Raycast(_backGorundCheckTransform.position, Vector3.down, out hitBack, 1.1f);
            RaycastHit hitCenter;
            Physics.Raycast(this.transform.position, Vector3.down, out hitCenter, 1.1f);

            if (hitFront.collider != null) {
                float groundSlope = Vector3.Angle(hitFront.normal, Vector3.up);//Calculates the slope of the ground underneath the player
                if (groundSlope < 50) {
                    _currentNumJumps = _maxNumJumps;
                    movementEnabled = true;
                }
            }
            else if (hitBack.collider != null) {
                float groundSlope = Vector3.Angle(hitBack.normal, Vector3.up);//Calculates the slope of the ground underneath the player
                if (groundSlope < 50) {
                    _currentNumJumps = _maxNumJumps;
                    movementEnabled = true;
                }
            }
            else if (hitCenter.collider != null) {
                float groundSlope = Vector3.Angle(hitCenter.normal, Vector3.up);//Calculates the slope of the ground underneath the player
                if (groundSlope < 50) {
                    _currentNumJumps = _maxNumJumps;
                    movementEnabled = true;
                }
            }
            else {
                Debug.Log("hit nothing");
            }
        }
    }
}
