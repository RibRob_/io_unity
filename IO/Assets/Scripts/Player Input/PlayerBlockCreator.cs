﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerBlockCreator : PlayerInput {

    //Private Members
    private string _createBlock = "";
    private string _destroyBlock = "";
    private string _shootBlock = "";
    private string _recallBlocks = "";
    private string _selectBlock = "";
    [SerializeField]
    private GameObject _cameraGO;
    [SerializeField]
    private Transform _blockSpawnPoint;
    [SerializeField]
    private CustomFPSCamera _customFPScam;
    //private Queue<Block> _blockQueue = new Queue<Block>();//NOTE: Maybe have the block script in the queue instead to make it more efficient?
    private int _blockLimit = 5;
    private int _numBlocksActive = 0;
    public int selectedBlock = 0;
    [SerializeField]
    private GameObject[] _blocks = new GameObject[3];
    [SerializeField]
    private float _shootForce = 10f;
    private float _blockWait = 0.2f;
    private float _createBlockTimer = 0;
    private float _shootBlockTimer = 0;
    private float _destroyBlockTimer = 0;
    private bool _creatingBlocksEnabled = true;
    private bool _destroyingBlocksEnabled = true;
    private bool _shootingBlocksEnabled = true;
    private bool _recallBlocksEnabled = true;
    private float _createBlockDistance = 10f;
    private List<Block> _blockList = new List<Block>();
    
    //Protected Members

    //Public Members
    public UnityEvent ueRecall;
    public UnityEvent ueDestoryOldest;

    //DO NOT UNCOMMENT. THIS BREAKS THE INPUT UPDATING.
    //private void Start(){
    //}

    void FixedUpdate() {
        _CheckSelectedBlock();
        _CreateBlock();
        _ShootBlock();
        _DestroyBlock();
        _SelectBlock();
        pTrackTimers();
    }

    protected override void pTrackTimers(){
        if (_createBlockTimer > 0) {
            _createBlockTimer -= Time.deltaTime;
        }
        if (_destroyBlockTimer > 0) { 
            _destroyBlockTimer -= Time.deltaTime;
        }
        if (_shootBlockTimer > 0) {
            _shootBlockTimer -= Time.deltaTime;
        }
        if (_destroyBlockTimer > 0) {
            _destroyBlockTimer -= Time.deltaTime;
        }
    }

    private void _CreateBlock() {
        if ((Input.GetButtonDown(_createBlock) || Input.GetAxis(_createBlock) > 0.1) && _createBlockTimer <= 0 && _creatingBlocksEnabled) {
            RaycastHit hit = _customFPScam.GetHit(_createBlockDistance);

            if (hit.collider != null) {
                Vector3 location = hit.point;
                location.y += 0.5f;
                GameObject blockClone = Instantiate(_blocks[selectedBlock], location, this.transform.localRotation);
                _createBlockTimer = _blockWait;
                _numBlocksActive++;
                _blockList.Add(blockClone.GetComponent<Block>());
                _CheckBlocksActive();
            }
        }
    }

    /// <summary>
    /// Checks the blocks active.
    /// </summary>
    private void _CheckBlocksActive() {

        if (_numBlocksActive > _blockLimit) {//Destroy the oldest block and replace it with the second oldest
            _blockList[0].Deconstruct();
            _blockList.RemoveAt(0);
            _numBlocksActive--;
        }

    }

    private void _ShootBlock() {
        if (Input.GetButtonDown(_shootBlock) && _shootBlockTimer <= 0 && _shootingBlocksEnabled) {
            _shootBlockTimer = _blockWait;

            GameObject blockClone = Instantiate(_blocks[selectedBlock], _blockSpawnPoint.position, this.transform.localRotation);
                blockClone.GetComponent<Block>().ueDeactivated.AddListener(BlockDeactivated);

            //if (_selectedBlock == 0) {
            //    blockClone.GetComponent<Block>().ueDeactivated.AddListener(BlockDeactivated);
            //}
            //if (_selectedBlock == 1) {
            //    blockClone.GetComponent<ElectricBock>().ueDeactivated.AddListener(BlockDeactivated);
            //}
            //if (_selectedBlock == 2) {
            //    blockClone.GetComponent<MagneticBlock>().ueDeactivated.AddListener(BlockDeactivated);
            //}
            Rigidbody rb = blockClone.GetComponent<Rigidbody>();
            rb.AddForce(_cameraGO.transform.forward * _shootForce);

            _numBlocksActive++;

            _blockList.Add(blockClone.GetComponent<Block>());
            _CheckBlocksActive();
        }
    }

    private void _DestroyBlock() {

        if ((Input.GetButtonDown(_destroyBlock) || Input.GetAxis(_destroyBlock) > 0.1f) && _destroyBlockTimer <= 0 && _destroyingBlocksEnabled) {

            _destroyBlockTimer = _blockWait;
            RaycastHit hit = _customFPScam.GetHit(1000);

            if (hit.collider != null){

                Block block = hit.collider.GetComponent<Block>();

                if (block != null) {
                    _blockList.Remove(block);
                    block.Deconstruct();
                    _numBlocksActive--;
                }
            }

        }
    }

    private void _SelectBlock(){
        if (!pcontrollerPresent && Input.GetAxis(_selectBlock) > 0.1) {
            selectedBlock++;
            _CheckSelectedBlock();
        }
        else if (!pcontrollerPresent && Input.GetAxis(_selectBlock) < -0.1) {
            selectedBlock--;
            _CheckSelectedBlock();
        }
    }

    private void _CheckSelectedBlock(){
        if (selectedBlock < 0) {
            selectedBlock = 2;
        }
        if (selectedBlock > 2) {
            selectedBlock = 0;
        }
    }

    public void BlockDeactivated() { 
    }


    public override void ChangeToKeyboardSupport() {
        pcontrollerPresent = false;
        _createBlock = "Right Mouse";
        _destroyBlock = "E";
        _recallBlocks = "Tab";
        _shootBlock = "Left Mouse";
        _selectBlock = "Mouse Wheel";
    }

    public override void ChangeToControllerSupport() {
        Debug.Log("Finding controller support...");
        pcontrollerPresent = true;

        if (pOS == "Windows") {
            _createBlock = "RightTrigger_XBx_Windows";
            _destroyBlock = "LeftTrigger_XBx_Windows";
            _recallBlocks = "LeftBumper_XBx_Windows";
            _shootBlock = "X_XBx_Windows";
            Debug.Log("Windows controller support enabled.");
        }
        else if (pOS == "Mac") {
            _createBlock = "RightTrigger_XBx_Mac";
            _destroyBlock = "LeftTrigger_XBx_Mac";
            _recallBlocks = "LeftBumper_XBx_Mac";
            _shootBlock = "X_XBx_Mac";
            Debug.Log("Mac controller support enabled.");
        }
        else if (pOS == "Linux") {
            _createBlock = "RightTrigger_XBx_Linux";
            _destroyBlock = "LeftTrigger_XBx_Linux";
            _recallBlocks = "LeftBumper_XBx_Linux";
            _shootBlock = "X_XBx_Linux";
            Debug.Log("Linux controller support enabled.");
        }
        else {
            Debug.Log("No controller support for this system.");
            ChangeToKeyboardSupport();
        }

    }
}
