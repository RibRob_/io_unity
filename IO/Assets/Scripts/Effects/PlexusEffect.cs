﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class PlexusEffect : MonoBehaviour{

    //Public Members
    public float maxDistance = 1.0f;
    public ParticleSystem thisParticleSystem;
    public LineRenderer lineRendererTemplate;

    //Private Members
    private float _maxDistanceSqr = 0f;
    private ParticleSystem.Particle[] _particles;
    private ParticleSystem.MainModule _thisParticleSystemMM;//MM = main module
    private List<LineRenderer> lineRenderers = new List<LineRenderer>();
    private Transform _transform;

    //Protected Members

    // Start is called before the first frame update
    void Start(){
        thisParticleSystem = GetComponent<ParticleSystem>();
        _thisParticleSystemMM = thisParticleSystem.main;
        _transform = transform;
    }

    private void LateUpdate(){
        _CheckParticleList();
        _DrawPlexus();
    }

    private void _CheckParticleList() {
        int maxParticles = _thisParticleSystemMM.maxParticles;

        if (_particles == null || _particles.Length < maxParticles) {
            Debug.Log("_particles array was not initialized, initializing...");
            _particles = new ParticleSystem.Particle[maxParticles];
        }
    }

    private void _DrawPlexus() {

        thisParticleSystem.GetParticles(_particles);
        int particleCount = thisParticleSystem.particleCount;

        _maxDistanceSqr = maxDistance * maxDistance;

        int lrIndex = 0;
        int lrCount = lineRenderers.Count;

        for (int i = 0; i < particleCount; i ++) {
            Vector3 particlePos1 = _particles[i].position;

            for (int j = i + 1; j < particleCount; j++) { //i + 1 prevents doubling connections
                Vector3 particlePos2 = _particles[j].position;
                float distanceSqr = Vector3.SqrMagnitude(particlePos1 - particlePos2);
                //SqrMagnitude less expensive than Distance & Magnitude (which have square root functions in them)

                if (distanceSqr <= _maxDistanceSqr){
                    LineRenderer lr;

                    if (lrIndex == lrCount) {
                        lr = Instantiate(lineRendererTemplate, _transform, false);
                        lineRenderers.Add(lr);

                        lrCount++;
                    }

                    lr = lineRenderers[lrIndex];

                    lr.enabled = true;

                    lr.SetPosition(0, particlePos1);
                    lr.SetPosition(1, particlePos2);

                    lrIndex++;
                }
            }
        }


        for (int i = lrIndex; i < lrCount; i++) {
            lineRenderers[i].enabled = false;
        }
    }
}
