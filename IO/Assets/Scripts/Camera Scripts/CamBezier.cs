﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CamBezier : MonoBehaviour
{
    [SerializeField]
    private GameObject[] worldPoints = new GameObject[1];

    private Vector3[] pathPoints;

    [SerializeField]
    private float pathTime;

    private float t;
    private Vector3 currentPos;

    void Awake()
    {
        t = 0.0f;
        pathPoints = new Vector3[worldPoints.Length];

        for (int i = 0; i < worldPoints.Length; i++)
        {
            pathPoints[i] = worldPoints[i].transform.position;
        }
    }

    public void GoToEnd()
    {
        StopCoroutine(ToStart());
        StartCoroutine(ToEnd());
    }

    public void GoToBeginning()
    {
        StopCoroutine(ToEnd());
        StartCoroutine(ToStart());
    }

    /// <summary>
    /// Finds the point along a bezier curve made up of points in an array, at a specific time
    /// </summary>
    /// <param name="p">Array of points (start, end, and control points)</param>
    /// <param name="t">Time</param>
    /// <returns></returns>
    private Vector3 FindBezierPoint(Vector3[] points, float t)
    {
        if (points == null)
        {
            return transform.position;
        }

        //Setting bezierPoint to the first point of the array causes it to return this value 
        //when the recursive calculations for the curve reach the point that the given array has only one Vector3
        Vector3 bezierPoint = points[0];

        if (points.Length > 1)
        {
            //Creates a new array of points containing all but the first point of the original array.
            Vector3[] newPoints = new Vector3[points.Length - 1];
            Array.Copy(points, 1, newPoints, 0, points.Length - 1);

            //Resize the first array so that it contains all but the last point of the original array.
            Array.Resize(ref points, points.Length - 1);

            //Recursively find the Vector3 of the point along the curve by finding the point t along each 
            //line segment created by the points
            bezierPoint = t * FindBezierPoint(newPoints, t) + (1 - t) * FindBezierPoint(points, t);
        }

        return bezierPoint;
    }


    //Move the camera to its starting position
    private IEnumerator ToStart()
    {
        while (t > 0)
        {
            t -= (Time.deltaTime / pathTime);
            currentPos = FindBezierPoint(pathPoints, t);
            transform.position = currentPos;
            transform.rotation = Quaternion.Lerp(worldPoints[0].transform.rotation, worldPoints[2].transform.rotation, t);
            yield return null;
        }

        t = 0;

        yield break;
    }

    //Move the camera to its final position
    private IEnumerator ToEnd()
    {
        while (t < 1)
        {
            t += (Time.deltaTime / pathTime);
            currentPos = FindBezierPoint(pathPoints, t);
            transform.position = currentPos;
            transform.rotation = Quaternion.Lerp(worldPoints[0].transform.rotation, worldPoints[2].transform.rotation, t);
            yield return null;
        }

        t = 1;

        yield break;
    }

    private void OnDrawGizmos()
    {
        //Draw the lines between the initial points
        Gizmos.color = Color.blue;
        for (int i = 0; i < worldPoints.Length - 1; i++)
        {
            Vector3 point1 = worldPoints[i].transform.position;
            Vector3 point2 = worldPoints[i + 1].transform.position;
            Gizmos.DrawLine(point1, point2);
        }

        //Converting the gameobjects' positions into Vector3 coordinates like in Awake, but it has to be done here as well to
        //get the points to show in the scene view in Unity, not just while the game is playing.
        Vector3[] points = new Vector3[worldPoints.Length];
        for (int i = 0; i < worldPoints.Length; i++)
        {
            points[i] = worldPoints[i].transform.position;
        }

        Gizmos.color = Color.cyan;
        //Draw the curve itself
        for (float i = 0.0f; i < 100; i++)
        {
            //Draw a line from 2 points on the curve that are 1/100 of the curve apart
            Gizmos.DrawLine(FindBezierPoint(points, i / 100), FindBezierPoint(points, (i + 1) / 100));
        }
    }
}
