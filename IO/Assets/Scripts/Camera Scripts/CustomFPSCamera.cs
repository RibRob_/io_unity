﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomFPSCamera : MonoBehaviour {

    //Private Members
    [SerializeField]
    private float _raycastRange = 200f;

    // Start is called before the first frame update
    void Start() {
        
    }

    /// <summary>
    /// Returns the object infront of where the player is facing.
    /// </summary>
    /// <returns>The hit.</returns>
    public RaycastHit GetHit(float range) {
        Physics.Raycast(this.transform.position, this.transform.forward, out RaycastHit hit, range);
        return hit;
    }
}
