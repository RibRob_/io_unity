﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinematicTrigger : MonoBehaviour {

    //Private Members

    //Protected Members
    [SerializeField]
    protected GameObject cinematic;

    //Public Members

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        
    }

    /// <summary>
    /// Uppon entering a given trigger, play a cinematic event.
    /// </summary>
    /// <param name="other">Other.</param>
    private void OnTriggerEnter(Collider other){
        if (other.tag == "Save") {
            cinematic.SetActive(true);
        }
    }
}
