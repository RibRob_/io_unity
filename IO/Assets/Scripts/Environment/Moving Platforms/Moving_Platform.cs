﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving_Platform : MonoBehaviour
{
    private bool isReversed, isPaused, isStopped;

    [SerializeField]
    private Transform startPoint;
    [SerializeField]
    private Transform endPoint;

    [SerializeField]
    private float speed;
    [SerializeField]
    private float pause;

    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        isStopped = true;
        isPaused = true;
        isReversed = false;
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPaused)
        {
            Move();
        }

    }


    private void Move()
    {
        if (!isReversed)
        {
            //If the path is going forward, increase the timer
            timer += (Time.deltaTime / speed);
        }
        else
        {
            //Otherwise, decrease the timer
            timer -= (Time.deltaTime / speed);
        }

        //Then move the platform appropriately
        transform.position = Vector3.Lerp(startPoint.position, endPoint.position, timer);
        

        if (timer <= 0 && isReversed)
        {
            //If the changed timer is <=0 and the path is going backwards, set the path to go forward and set timer to 0
            StartCoroutine(PauseMovement());
            isReversed = false;
            timer = 0;
        }
        else if (timer >= 1 && !isReversed)
        {
            //If the changed timer is >=1 and the path is going forward, set the path to go backward and set timer to 1
            StartCoroutine(PauseMovement());
            isReversed = true;
            timer = 1;
        }
        
    }

    //Pause the platform for a specified number of seconds
    private IEnumerator PauseMovement ()
    {
        isPaused = true;
        yield return new WaitForSeconds(pause);
        if (!isStopped)
            isPaused = false;
    }

    //Return the platform to its starting position and then stop its movement
    public void Pause ()
    {
        isStopped = true;
        isPaused = true;
        StopCoroutine(ReturnToStart());
        StartCoroutine(ReturnToStart());
    }

    public void Unpause ()
    {
        isStopped = false;
        isPaused = false;
    }

    //Return the platform to its starting position and reset it
    private IEnumerator ReturnToStart ()
    {
        isStopped = true;
        while (timer > 0)
        {
            timer -= (Time.deltaTime / (speed));
            transform.position = Vector3.Lerp(startPoint.position, endPoint.position, timer);
            yield return null;
        }

        timer = 0;
        if (isReversed)
            isReversed = false;

        yield break;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(startPoint.position, endPoint.position);
    }
}
