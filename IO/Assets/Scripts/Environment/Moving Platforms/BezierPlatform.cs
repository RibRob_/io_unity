﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BezierPlatform : MonoBehaviour
{
    [SerializeField]
    private GameObject[] worldPoints = new GameObject[1];

    private Vector3[] pathPoints;                                                

    [SerializeField]
    private float pathTime;
    [SerializeField]
    private float pause;

    private float t;
    private bool isReversed, isPaused, isStopped;
    private Vector3 currentPos;

    void Awake()
    {
        isStopped = true;
        isPaused = true;
        isReversed = false;
        t = 0.0f;
        pathPoints = new Vector3[worldPoints.Length];                          

        for (int i = 0; i < worldPoints.Length; i++)                              
        {
            pathPoints[i] = worldPoints[i].transform.position;                      
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPaused)
        {
            Move();
        }
        
    }

    private void Move()
    {
        t = FindT(t);
        currentPos = FindBezierPoint(pathPoints, t);
        transform.position = currentPos;
    }

    /// <summary>
    /// Increments the timer from 0 to 1 or decrements from 1 to 0
    /// </summary>
    /// <param name="timer">Timer variable</param>
    /// <returns></returns>
    private float FindT(float timer)
    {
        if (!isReversed)
        {
            //If the path is going forward, increase the timer
            timer += (Time.deltaTime / pathTime);
        }
        else
        {
            //Otherwise, decrease the timer
            timer -= (Time.deltaTime / pathTime);
        }

        if (timer <= 0 && isReversed)
        {
            //If the changed timer is <=0 and the path is going backwards, set the path to go forward and set timer to 0
            StartCoroutine(PauseMovement());
            isReversed = false;
            timer = 0;
        }
        else if (timer >= 1 && !isReversed)
        {
            //If the changed timer is >=1 and the path is going forward, set the path to go backward and set timer to 1
            StartCoroutine(PauseMovement());
            isReversed = true;
            timer = 1;
        }

        return timer;
    }

    /// <summary>
    /// Finds the point along a bezier curve made up of points in an array, at a specific time
    /// </summary>
    /// <param name="p">Array of points (start, end, and control points)</param>
    /// <param name="t">Time</param>
    /// <returns></returns>
    private Vector3 FindBezierPoint(Vector3[] points, float t)
    {
        if (points == null)
        {
            return transform.position;
        }

        //Setting bezierPoint to the first point of the array causes it to return this value 
        //when the recursive calculations for the curve reach the point that the given array has only one Vector2
        Vector3 bezierPoint = points[0];

        if (points.Length > 1)
        {
            //Creates a new array of points containing all but the first point of the original array.
            Vector3[] newPoints = new Vector3[points.Length - 1];
            Array.Copy(points, 1, newPoints, 0, points.Length - 1);

            //Resize the first array so that it contains all but the last point of the original array.
            Array.Resize(ref points, points.Length - 1);

            //Recursively find the Vector2 of the point along the curve by finding the point t along each 
            //line segment created by the points
            bezierPoint = t * FindBezierPoint(newPoints, t) + (1 - t) * FindBezierPoint(points, t);
        }

        return bezierPoint;
    }

    //Pause the platform for a specified number of seconds
    private IEnumerator PauseMovement()
    {
        isPaused = true;
        yield return new WaitForSeconds(pause);
        if (!isStopped)
            isPaused = false;
    }

    //Return the platform to its starting position and then stop its movement
    public void Pause()
    {
        isStopped = true;
        isPaused = true;
        StopCoroutine(ReturnToStart());
        StartCoroutine(ReturnToStart());
    }

    public void Unpause()
    {
        isStopped = false;
        isPaused = false;
    }

    //Return the platform to its starting position and reset it
    private IEnumerator ReturnToStart()
    {
        isStopped = true;
        while (t > 0)
        {
            t -= (Time.deltaTime / (pathTime));
            currentPos = FindBezierPoint(pathPoints, t);
            transform.position = currentPos;
            yield return null;
        }

        t = 0;
        if (isReversed)
            isReversed = false;

        yield break;
    }

    private void OnDrawGizmos()
    {
        //Draw the lines between the initial points
        Gizmos.color = Color.blue;
        for (int i = 0; i < worldPoints.Length - 1; i++)
        {
            Vector3 point1 = worldPoints[i].transform.position;
            //point1.y = yVal;
            Vector3 point2 = worldPoints[i + 1].transform.position;
            //point2.y = yVal;
            Gizmos.DrawLine(point1, point2);
        }

        //Converting the gameobjects' positions into Vector2 coordinates like in Awake, but it has to be done here as well to
        //get the points to show in the scene view in Unity, not just while the game is playing.
        Vector3[] points = new Vector3[worldPoints.Length];
        for (int i = 0; i < worldPoints.Length; i++)
        {
            points[i] = worldPoints[i].transform.position;
        }

        Gizmos.color = Color.cyan;
        //Draw the curve itself
        for (float i = 0.0f; i < 100; i++)
        {
            //Draw a line from 2 points on the curve that are 1/100 of the curve apart
            Gizmos.DrawLine(FindBezierPoint(points, i / 100), FindBezierPoint(points, (i + 1) / 100));
        }
    }
}
