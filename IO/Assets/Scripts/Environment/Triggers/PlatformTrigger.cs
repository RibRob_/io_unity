﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformTrigger : MonoBehaviour
{
    [SerializeField]
    private List<Moving_Platform> platforms;

    //A list of all blocks activating the trigger
    private List<GameObject> triggerBlocks = new List<GameObject>();
    protected bool hasBlock;

    protected void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ElectricBock>() != null)
        {
            triggerBlocks.Add(other.gameObject);
            foreach (Moving_Platform p in platforms)
            {
                p.Unpause();
            }
            hasBlock = true;
            StartCoroutine(CheckList(triggerBlocks));
        }
    }

    protected void Start()
    {
        hasBlock = false;
    }

    protected IEnumerator CheckList(List<GameObject> list)
    {
        while (hasBlock)
        {
            // Check the list of blocks once per second to clear it of destroyed blocks
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] == null || list[i].activeInHierarchy == false)
                {
                    list.RemoveAt(i);
                    i--;
                }
            }

            //If there are no blocks activating the trigger, stop the platform
            if (list.Count == 0)
            {
                hasBlock = false;
                foreach (Moving_Platform p in platforms)
                {
                    p.Pause();
                }
            }

            yield return new WaitForSeconds(0.5f);
        }

        yield break;

    }
}
