﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleTriggers : MonoBehaviour
{
    [SerializeField]
    private Moving_Platform platform;

    //A list of all blocks activating the trigger
    //This static list is shared amongst all triggers in the scene and is only for platforms with more than one trigger
    private static List<GameObject> triggerBlocks = new List<GameObject>();
    private bool hasBlock;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ElectricBock>() != null)
        {
            triggerBlocks.Add(other.gameObject);
            platform.Unpause();
            hasBlock = true;
            StartCoroutine(CheckList(triggerBlocks));
        }
    }

    private void Start()
    {
        hasBlock = false;
    }

    private IEnumerator CheckList(List<GameObject> list)
    {
        while (hasBlock)
        {
            // Check the list of blocks once per second to clear it of destroyed blocks
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] == null || list[i].activeInHierarchy == false)
                {
                    list.RemoveAt(i);
                    i--;
                }
            }

            //If there are no blocks activating the trigger, stop the platform
            if (list.Count == 0)
            {
                hasBlock = false;
                platform.Pause();
            }

            yield return new WaitForSeconds(0.5f);
        }

        yield break;
    }
}