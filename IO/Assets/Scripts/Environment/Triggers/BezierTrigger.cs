﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierTrigger : MonoBehaviour
{
    [SerializeField]
    private BezierPlatform platform;

    //A list of all blocks activating the trigger
    private List<GameObject> triggerBlocks = new List<GameObject>();

    private bool hasBlock;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collision with trigger");
        if (other.GetComponent<ElectricBock>() != null)
        {
            Debug.Log("Electric Block Detected");
            triggerBlocks.Add(other.gameObject);
            platform.Unpause();
            hasBlock = true;
            StopCoroutine(CheckList(triggerBlocks));
            StartCoroutine(CheckList(triggerBlocks));
        }
    }

    private void Start()
    {
        hasBlock = false;
    }

    private IEnumerator CheckList(List<GameObject> list)
    {
        while (hasBlock)
        {
            // Check the list of blocks once per second to clear it of destroyed blocks
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] == null || list[i].activeInHierarchy == false)
                {
                    list.RemoveAt(i);
                    i--;
                }
            }

            //If there are no blocks activating the trigger, stop the platform
            if (list.Count == 0)
            {
                hasBlock = false;
                platform.Pause();
            }
    
            yield return new WaitForSeconds(0.5f);
        }

        yield break;

    }
}
