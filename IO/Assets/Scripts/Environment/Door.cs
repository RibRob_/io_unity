﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField]
    private Transform spawnPoint;

    [SerializeField]
    private int spawnID;

    [SerializeField]
    private StartingPoint player;

    private void OnTriggerEnter(Collider other)
    {
        //If the player enters the door, move the player to the appropriate position and set the player's respawn point
        if (other.gameObject == player.gameObject)
        {
            other.transform.position = spawnPoint.position;
            other.transform.rotation = spawnPoint.rotation;
            player.SetSpawn(spawnID);
        }
    }
}
