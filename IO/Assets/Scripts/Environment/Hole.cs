﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour
{
    [SerializeField]
    private StartingPoint player;

    private void OnTriggerEnter(Collider other)
    {
        //If the player hits the trigger, respawn the player
        if (other.gameObject == player.gameObject)
        {
            player.Respawn();
        }
    }
}
