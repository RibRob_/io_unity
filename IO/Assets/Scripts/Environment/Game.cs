﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    [SerializeField]
    private List<Transform> warpPoints;

    [SerializeField]
    private GameObject player;

    private IEnumerator LoadLevel(string level)
    {
        yield return SceneManager.LoadSceneAsync(level, LoadSceneMode.Additive);
    }

    private void Awake()
    {
        for (int i = 1; i <= 3; i++)
        {
            StartCoroutine(LoadLevel("Level" + i));
        }
    }

    private void Update()
    {
        Cheat();
    }

    private void Cheat()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            for (int i = 1; i <= warpPoints.Count; i++)
            {
                if (Input.GetKey(KeyCode.Alpha0 + i))
                {
                    player.transform.position = warpPoints[i - 1].position;
                    player.transform.rotation = warpPoints[i - 1].rotation;
                }
            }
        }
    }
}
