﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script is used to have objects look at and track othe objects

public class Lookat : MonoBehaviour {

    //Private Members
    [SerializeField]
    private Transform _target;

    //Protected Members

    //Public Members

    // Start is called before the first frame update
    void Start() {
    }

    private void Update() {//Do not have this as FixedUpdate. Makes cinematics not look as smooth.
        transform.LookAt(_target);
    }
}
