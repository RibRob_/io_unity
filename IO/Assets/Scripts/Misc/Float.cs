﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Float : MonoBehaviour{

    //Public Members
    public Transform transToFloat;
    public float amplitude = 0.5f;
    public float speed = 1f;

    //Private Members

    //Protected Members

    // Start is called before the first frame update
    void Start(){
        
    }

    // Update is called once per frame
    void Update(){
        Vector3 newPosition = new Vector3();
        newPosition.y = Mathf.Sin(Time.timeSinceLevelLoad) * amplitude;
        transToFloat.position += newPosition;
    }
}
