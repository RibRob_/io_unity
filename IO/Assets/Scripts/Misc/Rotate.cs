﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

    //Public Members

    //Private Members
    [SerializeField] private Transform obj;
    [SerializeField] private float _speed = 1f;

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        Vector3 newRot = new Vector3(0, 0, obj.transform.rotation.z + (Time.deltaTime * _speed));
    }

}
