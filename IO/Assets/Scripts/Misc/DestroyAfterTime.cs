﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {

    //Public Members
    public float time = 1;

    //Private Members

    //Protected Members

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        _TrackTime();
        _Death();
    }

    private void _TrackTime() {
        if (time > 0) {
            time -= Time.deltaTime;
        }
    }

    private void _Death() {
        if (time <= 0) {
            Destroy(this.gameObject);
        }
    }
}
