﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script was made to handle the initial setup, like setting the framerate.

public class InitialSetup : MonoBehaviour {

    //Public Members
    public PlayerMovement playerMovement;

    //Private Members

    //Protected Members

    // Start is called before the first frame update
    void Start() {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
        playerMovement.Lock();
    }

    // Update is called once per frame
    void Update() {
        
    }
}
