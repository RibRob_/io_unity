﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableAfterTime : MonoBehaviour {

    //Public Members
    public float currentTime = 1;
    public float timerReset = 1;

    //Private Members

    //Protected Members

    // Update is called once per frame
    void Update() {
        _TrackTime();
        _Death();
    }

    private void _TrackTime() {
        if (currentTime > 0) {
            currentTime -= Time.deltaTime;
        }
    }

    private void _Death() {
        if (currentTime <= 0) {
            this.gameObject.SetActive(false);
        }
    }

    public void ResetTimer() {
        currentTime = timerReset;
    }
}
