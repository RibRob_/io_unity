﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class EnemySpawner : MonoBehaviour{

    //Public Members
    public float spawnTimerReset = 5f;
    public float spawnTimer = 0f;
    public int numEnemiesPresent = 0;
    public int enemyLimit = 5;
    public GameObject enemyTemplate;
    public Transform enemiesParent;
    public EnemyManager enemyManager;
    public MeshRenderer meshRenderer;
    public string sceneName = "";
    public bool sceneLoaded = false;
    public Player player;

    //Private Members
    private Vector3 _position;
    private Quaternion _rotation;

    //Protected Members

    // Start is called before the first frame update
    void Start(){
        _position = transform.position;
        _rotation = transform.rotation;
        meshRenderer.enabled = false;
    }

    public virtual void CheckSceneLoaded() {
        if (SceneManager.GetSceneByName(sceneName).isLoaded) {
            sceneLoaded = true;
        }
        else {
            sceneLoaded = false;
        }
    }

    public virtual void TrackTimers(){
        if (spawnTimer > 0) {
            spawnTimer -= Time.deltaTime;
        }
    }

    public virtual void Spawn() {
        if (spawnTimer <= 0) {

            spawnTimer = spawnTimerReset;

            if (numEnemiesPresent < enemyLimit) {
                GameObject enemyClone = Instantiate(enemyTemplate, _position, _rotation, enemiesParent);
                enemyManager.AddEnemy(enemyClone.GetComponent<Enemy>());
                enemyClone.GetComponent<Enemy>().player = player;

                numEnemiesPresent++;
            }
        }
    }
}
