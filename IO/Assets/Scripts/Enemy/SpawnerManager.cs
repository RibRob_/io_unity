﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour{

    //Public Members
    public List<EnemySpawner> enemySpawners = new List<EnemySpawner>();

    //Private Members

    //Protected Members

    // Start is called before the first frame update
    void Start(){
    }

    // Update is called once per frame
    void Update(){
        foreach (EnemySpawner spawner in enemySpawners) {
            spawner.TrackTimers();

            if (spawner.sceneLoaded) {
                spawner.Spawn();
            }
            else {
                spawner.CheckSceneLoaded();
            }
        }
    }
}
