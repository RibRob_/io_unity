﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyManager : MonoBehaviour{

    //Public Members
    public List<Enemy> enemies = new List<Enemy>();
    public Transform enemyParent;
    public Player player;

    //Private Members

    //Protected Members

    // Start is called before the first frame update
    void Start(){
        
    }

    private void FixedUpdate() {

        foreach (Enemy e in enemies) {
            if (e != null) {
                e.TrackTimers();
                e.CheckHealth();
                e.Act();
            }
            else {
                Debug.Log("Enemy doesn't exist. Removing instance.");
                enemies.Remove(e);
                break;
            }
        }

    }

    public void AddEnemy(Enemy e) {
        enemies.Add(e);
        e.player = player;
        Debug.Log("Enemy Added");
    }
}
