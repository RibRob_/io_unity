﻿//Rob Harwood
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : MonoBehaviour{

    //Public Members
    public int currentHP = 20;
    public int maxHP = 20;
    public Renderer enemyRenderer;
    public float agroRange = 2f;
    public GameObject playerGO;
    public NavMeshAgent navMeshAgent;
    public Player player;
    public Collider collider;
    

    //Private Members

    //Protected Members
    protected Transform pTransfrom;
    protected float pDeathTimer = 0f;
    //protected Animator crawlerAnim;

    // Start is called before the first frame update
    void Start(){
        //crawlerAnim = GetComponent<Animator>();
        collider = GetComponent<Collider>();
        enemyRenderer = this.gameObject.GetComponent<Renderer>();
        enemyRenderer.material.SetFloat("Vector1_B5275CBD", 0);
        //enemyMat.SetFloat("Vector1_B5275CBD", 0);
        navMeshAgent = GetComponent<NavMeshAgent>();
        pTransfrom = transform;
        playerGO = GameObject.FindGameObjectWithTag("Player");

    }

    public virtual void TrackTimers() { 
    }

    public void CheckHealth() {
        if (currentHP <= 0){
            pDeathTimer += Time.deltaTime;
            enemyRenderer.material.SetFloat("Vector1_B5275CBD", pDeathTimer);

            if (collider.enabled) {
                collider.enabled = false;
            }

            if (pDeathTimer >= 2){
                PDie();
            }
        }
    }

    public virtual void Act() { 

    }

    protected virtual void PIdle() { 

    }

    protected virtual void PAgro() { 

    }

    protected virtual void PAttack() { 

    }

    protected virtual void PDie(){
    enemyRenderer.material.SetFloat("Vector1_B5275CBD", 1);
        Destroy(this.gameObject);
    }

    public void TakeDamage(object value) {
        int damage = int.Parse(value.ToString());
        currentHP -= damage;

    }
}
