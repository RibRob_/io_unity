﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : Enemy{

    //Public Members
    public float attackTimerReset = 3f;
    public float currentAttackTimer = 0f;
    public int attackDamage = 10;

    //Private Members
    private float distance;

    //Protected Members
        

    public override void TrackTimers() {
        if (currentAttackTimer > 0) {
            currentAttackTimer -= Time.deltaTime;
        }
    }

    public override void Act() {

        //Calculate distance from player
        Vector3 playerPos = playerGO.transform.position;
        Vector3 enemyPos = pTransfrom.position;
        distance = Vector3.Distance(enemyPos, playerPos);

        //If player is close use Agro behavior, else Idle
        if (distance <= agroRange) {
            PAgro();
            //crawlerAnim.SetBool("isWalking", true);
        }
        else {
            PIdle();
            //crawlerAnim.SetBool("isWalking", false);
        }
    }

    protected override void PIdle() {

    }

    protected override void PAgro() {
        if (currentHP > 0) {

            if (distance > 2) {
                navMeshAgent.SetDestination(playerGO.transform.position);
            }

            if (distance <= 2 && currentAttackTimer <= 0) {
                Debug.Log("Attacking!!!");
                player.TakeDamage(attackDamage);
                currentAttackTimer = attackTimerReset;
            }
        }
    }
}
