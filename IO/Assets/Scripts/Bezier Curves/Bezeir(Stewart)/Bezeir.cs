﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bezeir : MonoBehaviour
{
    //variables for bezier curve
    public LineRenderer lr;
    private int numBezPoints = 50;
    private Vector3[] positions = new Vector3[50];
    public Transform p0;
    public Transform p1;
    public Transform p2;
    public float t;

    //variables for sideCamera movement on bezier curve
    //not sure how to implement atm
    //some variables might not be needed
    public Camera sCam;
    public Transform sideCamera;
    public float startTime;
    public float sideCameraSpeed = .1f;
    bool isdone = false;


    // Start is called before the first frame update
    void Start()
    {
        sCam.enabled = true;
        startTime = Time.deltaTime;
        //sideCamera starts at p0
        //sideCamera.transform.position = p0.position;
        //DrawQuadraticBezCurve();
        lr = GetComponent<LineRenderer>();
        lr.positionCount = numBezPoints;
        //CalculateQuatraticBezierCurve(t, p0.position, p1.position, p2.position);
        //DrawQuadraticBezCurve();

    }

    // Update is called once per frame
    void Update()
    {
        //DrawLinearBezeirCurve();
        //DrawQuadraticBezCurve();
        CameraFollowBezier();
       

    }

    //used to test line between p0 and p1
    private void DrawLinearBezeirCurve()
    {

        for(int i = 1; i < numBezPoints + 1; i++)
        {
            t = i / (float)numBezPoints;
            positions[i - 1] = CalculateBezeirLine(t, p0.position, p1.position);
            
        }
        lr.SetPositions(positions);
    }

    //draws bezier curve between p0, p1, p2
    private void DrawQuadraticBezCurve()
    {
        //distanceCovered = (Time.deltaTime * startTime) * sideCameraSpeed;
        //Debug.Log(distanceCovered);
        //distanceCompleted = distanceCovered / bezierLength;
       //Debug.Log(distanceCompleted);
        for (int i = 1; i < numBezPoints + 1; i++)
        {

            t = i / (float)numBezPoints;
            positions[i - 1] = CalculateQuatraticBezierCurve(t, p0.position, p1.position, p2.position);

        }
        //lr is used for visual
        //will get rid of later
        lr.SetPositions(positions);

    }

    //calculates line from p0, p1
    private Vector3 CalculateBezeirLine(float t, Vector3 p0, Vector3 p1)
    {
        return p0 + t * (p1 - p0);
    }

    //calculates a quatratic bezier curve
    private Vector3 CalculateQuatraticBezierCurve(float t, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        // return = (1-t)2P0 + 2(1-t)tP1 + t2P2
        //            u           u        tt
        //   p  =    uu * P0 + 2 * u * t * p1 + tt * p2
        // above is simplified formula

        //startTime was recently added to use Time.deltaTime.

        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        Vector3 p = uu * p0;
        p += 2 * u * t * p1;
        p += tt * p2;

        return p;

       
    }

    private void CameraFollowBezier()
    {
        startTime += Time.deltaTime * sideCameraSpeed;
        if (startTime < 1)
        {
            //startTime += Time.deltaTime * sideCameraSpeed;
            float u = 1 - startTime;
            float tt = startTime * startTime;
            float uu = u * u;
            sideCamera.position = uu * p0.position + 2 * u * startTime * p1.position + tt * p2.position;
        }
        if (startTime >= 1)
        {
            sCam.enabled = false;
        }

    }


}
