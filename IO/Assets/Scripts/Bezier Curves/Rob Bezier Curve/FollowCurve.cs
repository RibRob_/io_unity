﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script is used on a cinematic camera to have the camera follow along a smooth curve.

public class FollowCurve : MonoBehaviour {

    //Private Members
    [SerializeField]
    private Transform[] _routesToFollow;

    [SerializeField]
    private float _speed = 0.5f;

    [SerializeField]
    private GameObject _cinematicEvent;

    [SerializeField]
    private bool _looping = false;

    private Vector3 _camPosition;
    private int _routesDone = 0;
    private float _timePassed = 0f;
    private bool _coroutineStarted = false;

    //Protected Members

    //Public Members

    // Start is called before the first frame update
    void Start() {
        _camPosition = this.transform.position;
    }

    private void FixedUpdate() {
        if (!_coroutineStarted) {
            StartCoroutine(FollowRoute());
        }
    }

    /// <summary>
    /// Has the camera, or any desired object follow a designated route.
    /// </summary>
    /// <returns>The route.</returns>
    private IEnumerator FollowRoute() {
        _coroutineStarted = true;

        while (_timePassed < 1) {
            _timePassed += Time.deltaTime * _speed;

            //Formula for creating a cubic bezier curve:
            //B(t) = ((1 - t)^3) * P0 + 3 * ((1 - t)^2) * t * P1 + 
            //3 * (1 - t) * (t^2) * P2 + (t^3) * P3, 0 <= t <= 1
            //t = time, P = point, B = Bezier Curve
            _camPosition = 
                Mathf.Pow(1 - _timePassed, 3) * _routesToFollow[_routesDone].GetChild(0).position +
                3 * Mathf.Pow(1 - _timePassed, 2) * _timePassed * _routesToFollow[_routesDone].GetChild(1).position +
                3 * (1 - _timePassed) * Mathf.Pow(_timePassed, 2) * _routesToFollow[_routesDone].GetChild(2).position + 
                Mathf.Pow(_timePassed, 3) * _routesToFollow[_routesDone].GetChild(3).position;

            transform.position = _camPosition;
            yield return new WaitForEndOfFrame();
        }

        _timePassed = 0;
        _routesDone++;

        //If the object is done following a set number of routes, set the cinematic to be inactive.
        if (_routesDone > _routesToFollow.Length - 1) {
            _routesDone = 0;
            if (!_looping) {
                _cinematicEvent.SetActive(false);
            }
        }

        _coroutineStarted = false;
    }
}
