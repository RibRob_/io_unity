﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierCurveRoute : MonoBehaviour {

    //This script is used to draw an in engine representation of a smooth, curved line for moving objects along,
    //typically for cinematics.

    //Private Members
    [SerializeField]
    private Transform[] _points = new Transform[4];
    private Vector3 _bezierCurve;

    //Protected Members

    //Public Members

    /// <summary>
    /// Draws the route and tranforms used to control it.
    /// </summary>
    private void OnDrawGizmos() {

        //Draws the route.
        for (float time = 0; time <= 1; time = time + 0.02f) {

            //Formula for creating a cubic bezier curve:
            //B(t) = ((1 - t)^3) * P0 + 3 * ((1 - t)^2) * t * P1 + 
            //3 * (1 - t) * (t^2) * P2 + (t^3) * P3, 0 <= t <= 1
            //t = time, P = point, B = Bezier Curve
            _bezierCurve =
                Mathf.Pow(1 - time, 3) * _points[0].position + 3 * Mathf.Pow(1 - time, 2) * time * _points[1].position +
                3 *  (1 - time) * Mathf.Pow(time, 2) * _points[2].position + Mathf.Pow(time, 3) * _points[3].position;

            Gizmos.DrawSphere(_bezierCurve, 0.1f);
        }

        //Draw the modifiers' transforms
        Vector3 cubeSize = new Vector3(0.1f, 0.1f, 0.1f);
        Gizmos.DrawCube(_points[1].position, cubeSize);
        Gizmos.DrawCube(_points[2].position, cubeSize);

        //Draws the lines to show the transforms that control the direction of the route
        Gizmos.DrawLine(
            new Vector3(_points[0].position.x, _points[0].position.y, _points[0].position.z),
            new Vector3(_points[1].position.x, _points[1].position.y, _points[1].position.z)
            );

        Gizmos.DrawLine(
            new Vector3(_points[2].position.x, _points[2].position.y, _points[2].position.z),
            new Vector3(_points[3].position.x, _points[3].position.y, _points[3].position.z)
            );
    }
}
