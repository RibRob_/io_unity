﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MegBezierCurve : MonoBehaviour
{

    [SerializeField]
    private GameObject[] worldPoints = new GameObject[1];

    private Vector2[] pathPoints;

    [SerializeField]
    private float pathTime;

    private float yVal, t;
    private bool reversed;
    private Vector2 bPoint;
    private Vector3 currentPos;

    // Start is called before the first frame update
    void Awake()
    {
        yVal = transform.position.y;
        reversed = false;
        t = 0.0f;
        pathPoints = new Vector2[worldPoints.Length];
        for (int i = 0; i < worldPoints.Length; i++)
        {
            pathPoints[i] = ConvertToV2(worldPoints[i].transform.position);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Calculate t
        t = FindT(t);

        //Calculate x and z positions based on the curve at time t
        bPoint = FindBezierPoint(pathPoints, t);

        //Set position to match the curve
        currentPos = ConvertToV3(bPoint, yVal);

        Debug.Log(t);
        Debug.Log(bPoint);
        Debug.Log(currentPos);

        transform.position = currentPos;
    }

    /// <summary>
    /// Increments the timer from 0 to 1 or decrements from 1 to 0
    /// </summary>
    /// <param name="timer">Timer variable</param>
    /// <returns></returns>
    private float FindT(float timer)
    {
        if (!reversed)
        {
            //If the path is going forward, increase the timer
            timer += (Time.deltaTime / pathTime);
        }
        else
        {
            //Otherwise, decrease the timer
            timer -= (Time.deltaTime / pathTime);
        }

        if (timer <= 0 && reversed)
        {
            //If the changed timer is <=0 and the path is going backwards, set the path to go forward and set timer to 0
            reversed = false;
            timer = 0;
        }
        else if (timer >= 1 && !reversed)
        {
            //If the changed timer is >=1 and the path is going forward, set the path to go backward and set timer to 1
            reversed = true;
            timer = 1;
        }

        return timer;
    }

    /// <summary>
    /// Finds the point along a bezier curve made up of points in an array, at a specific time
    /// </summary>
    /// <param name="p">Array of points (start, end, and control points)</param>
    /// <param name="t">Time</param>
    /// <returns></returns>
    private Vector2 FindBezierPoint(Vector2[] points, float t)
    {
        if (points == null)
        {
            return transform.position;
        }

        //Setting bezierPoint to the first point of the array causes it to return this value 
        //when the recursive calculations for the curve reach the point that the given array has only one Vector2
        Vector2 bezierPoint = points[0];

        if (points.Length > 1)
        {
            //Creates a new array of points containing all but the first point of the original array.
            Vector2[] newPoints = new Vector2[points.Length - 1];
            Array.Copy(points, 1, newPoints, 0, points.Length - 1);

            //Resize the first array so that it contains all but the last point of the original array.
            Array.Resize(ref points, points.Length - 1);

            //Recursively find the Vector2 of the point along the curve by finding the point t along each 
            //line segment created by the points
            bezierPoint = t * FindBezierPoint(newPoints, t) + (1 - t) * FindBezierPoint(points, t);
        }

        return bezierPoint;
    }

    /// <summary>
    /// Converts a Vector2 point to a Vector3 point with a set y-value
    /// </summary>
    /// <param name="point">Point to be converted</param>
    /// <param name="yPos">Y value</param>
    /// <returns></returns>
    private Vector3 ConvertToV3(Vector2 point, float yPos)
    {
        Vector3 newPoint;
        newPoint.x = point.x;
        newPoint.y = yPos;
        newPoint.z = point.y;

        return newPoint;
    }

    /// <summary>
    /// Converts a Vector3 point to a Vector2 point using the Vector3's x and z values
    /// </summary>
    /// <param name="point">Point to be converted</param>
    /// <returns></returns>
    private Vector2 ConvertToV2(Vector3 point)
    {
        Vector2 newPoint;
        newPoint.x = point.x;
        newPoint.y = point.z;
        return newPoint;
    }

    private void OnDrawGizmos()
    {
        //Draw the lines between the initial points
        Gizmos.color = Color.blue;
        for (int i = 0; i < worldPoints.Length - 1; i++)
        {
            Vector3 point1 = worldPoints[i].transform.position;
            point1.y = yVal;
            Vector3 point2 = worldPoints[i + 1].transform.position;
            point2.y = yVal;
            Gizmos.DrawLine(point1, point2);
        }

        //Converting the gameobjects' positions into Vector2 coordinates like in Awake, but it has to be done here as well to
        //get the points to show in the scene view in Unity, not just while the game is playing.
        Vector2[] points = new Vector2[worldPoints.Length];
        for (int i = 0; i < worldPoints.Length; i++)
        {
            points[i] = ConvertToV2(worldPoints[i].transform.position);
        }

        Gizmos.color = Color.cyan;
        //Draw the curve itself
        for (float i = 0.0f; i < 100; i++)
        {
            //Draw a line from 2 points on the curve that are 1/100 of the curve apart
            Gizmos.DrawLine(ConvertToV3(FindBezierPoint(points, i / 100), yVal), ConvertToV3(FindBezierPoint(points, (i + 1) / 100), yVal));
        }
    }
}
