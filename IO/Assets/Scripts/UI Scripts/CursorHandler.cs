﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorHandler : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
        LockCursor();
    }

    public void LockCursor() {
        Debug.Log("Lock event called...");

        Debug.Log("Locking cursor...");
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void UnlockCursor() {
        Debug.Log("Unlock event called...");

        Debug.Log("Unlocking cursor...");
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
