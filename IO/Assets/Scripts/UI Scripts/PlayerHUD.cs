﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour{

    //Public Members
    public Player player;
    public Slider healthBar;

    //Private Members

    //Protected Members

    // Start is called before the first frame update
    void Start() {
        healthBar.value = 100;
    }

    // Update is called once per frame
    void Update() {
        _UpdateHealthBar();
    }

    private void _UpdateHealthBar() {
        healthBar.value = player.currentHP;
    }
}
