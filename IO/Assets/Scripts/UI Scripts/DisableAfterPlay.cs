﻿//Rob Harwood

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class DisableAfterPlay : MonoBehaviour{

    //Public Members
    public VideoPlayer videoPlayer;
    public GameObject rawImage;
    public float timer = 5f;
    public GameObject mainMenu;
    public GameObject background;
    public float alphaTimer = 1f;

    //Private Members

    //Protected Members


    // Start is called before the first frame update
    void Start(){
        mainMenu.SetActive(false);

        if (!rawImage.activeInHierarchy) {
            rawImage.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update(){
        TrackTimers();

        if (!videoPlayer.isPlaying && timer <= 0){
            background.SetActive(true);
            rawImage.SetActive(false);
        }
    }

    private void TrackTimers() {
        if (timer > 0) {
            timer -= Time.deltaTime;
        }
    }

}
