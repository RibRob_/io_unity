﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadialMenuHandler : PlayerInput {

    //Public Members
    public string joyClick = "";
    public string joyRotateHoriz = "";
    public string joyRotateVert = "";
    public string mouseWheel = "";
    public float mouseInput = 0f;
    public PlayerBlockCreator playerBlockCreator;
    public GameObject radialMenu;
    public Transform arrowTransform;
    public Fade menuFade;
    public Transform[] options;
    public float sensitivity = 2f;

    //Private Members
    private float _angle = 0f;
    private bool _redInrease = false;
    private bool _blueInrease = false;
    private bool _whiteInrease = false;
    private float _curretnFadeTimer = 2f;
    private float _fadeTimerReset = 2f;

    //Protected Members

    // Start is called before the first frame update
    void Start() {
        pDetectOS();
        menuFade.speed = 2;
    }

    // Update is called once per frame
    void Update() {

        _TrackFadeTime();

        if (pcontrollerPresent) {
            _JoyClick();
        }
        else if (!pcontrollerPresent) {
            _MouseWheel();
        }
    }

    private void _TrackFadeTime() {
        if (_curretnFadeTimer > 0 && (Input.mouseScrollDelta.y == 0 || !Input.GetButton(joyClick))) {
            _curretnFadeTimer -= Time.deltaTime;
        }
    }

    private void _JoyClick() {
        if (Input.GetButton(joyClick)) {
            menuFade.fadeOut = false;
            menuFade.fadeIn = true;
            _JoyRotate();
        }
        else {
            if (_curretnFadeTimer <= 0) {
                menuFade.fadeOut = true;
                menuFade.fadeIn = false;
            }
        }
    }

    private void _MouseWheel() {
        if (Input.mouseScrollDelta.y != 0) {
            menuFade.fadeOut = false;
            menuFade.fadeIn = true;
            _curretnFadeTimer = _fadeTimerReset;

            _angle += Input.mouseScrollDelta.y * sensitivity;
            _RotateArrow();
            _MakeBlockSelection();
        }
        else {
            if (_curretnFadeTimer <= 0) {
                menuFade.fadeOut = true;
                menuFade.fadeIn = false;
            }
        }
    }

    private void _JoyRotate() {
        float rotHorizontal = Input.GetAxisRaw(joyRotateHoriz) * -1;
        float rotVertical = Input.GetAxisRaw(joyRotateVert) * -1;

        _angle = Mathf.Atan2(rotHorizontal, rotVertical) * Mathf.Rad2Deg;

        _RotateArrow();

        _MakeBlockSelection();
    }

    private void _RotateArrow() {
        Vector3 newRot = new Vector3(0, 0, _angle);
        arrowTransform.rotation = Quaternion.Euler(newRot);
    }

    private void _MakeBlockSelection() {
        if (_angle < -180) {
            _angle = 180;
        }
        else if (_angle > 180) {
            _angle = -180;
        }


        if (_angle < 60 && _angle > -60) {                          //Red
            playerBlockCreator.selectedBlock = 2;

            if (!_redInrease) {
                if (_whiteInrease) {
                    options[0].localScale = options[0].localScale / 1.2f;
                    _whiteInrease = false;
                }
                else if (_blueInrease) {
                    options[1].localScale = options[1].localScale / 1.2f;
                    _blueInrease = false;
                }

                options[2].localScale = options[2].localScale * 1.2f;
                _redInrease = true;

            }
        }
        else if (_angle >= 60 || _angle == -180) {                  //Blue
            playerBlockCreator.selectedBlock = 1;

            if (!_blueInrease) {
                if (_whiteInrease) {
                    options[0].localScale = options[0].localScale / 1.2f;
                    _whiteInrease = false;
                }
                else if (_redInrease) {
                    options[2].localScale = options[2].localScale / 1.2f;
                    _redInrease = false;
                }

                options[1].localScale = options[1].localScale * 1.2f;
                _blueInrease = true;

            }
        }
        else {                                                      //White
            playerBlockCreator.selectedBlock = 0;

            if (!_whiteInrease) {
                if (_blueInrease) {
                    options[1].localScale = options[1].localScale / 1.2f;
                    _blueInrease = false;
                }
                else if (_redInrease) {
                    options[2].localScale = options[2].localScale / 1.2f;
                    _redInrease = false;
                }

                options[0].localScale = options[0].localScale * 1.2f;
                _whiteInrease = true;

            }
        }
    }


    public override void ChangeToControllerSupport() {
        pcontrollerPresent = true;
        if (pOS == "Windows") {
            joyClick = "LeftJoyClick_XBx_Windows";
            joyRotateHoriz = "HorizontalMovement_XBx_Windows";
            joyRotateVert = "VerticalMovement_XBx_Windows";
        }
        else if (pOS == "Mac") {
            joyClick = "LeftJoyClick_XBx_Mac";
            joyRotateHoriz = "HorizontalMovement_XBx_Mac";
            joyRotateVert = "VerticalMovement_XBx_Mac";
        }
        else if (pOS == "Linux"){
            joyClick = "LeftJoyClick_XBx_Linux";
            joyRotateHoriz = "HorizontalMovement_XBx_Linux";
            joyRotateVert = "VerticalMovement_XBx_Linux";
        }
        else {
            pcontrollerPresent = false;
            ChangeToKeyboardSupport();
        }
    }

    public override void ChangeToKeyboardSupport() {
        pcontrollerPresent = false;
        mouseWheel = "Mouse Wheel";
    }
}
