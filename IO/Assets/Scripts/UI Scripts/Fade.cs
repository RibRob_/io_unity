﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour{

    //Public Members
    public CanvasGroup canvasGroup;
    public bool fadeOut = true;
    public bool fadeIn = false;
    public GameObject toSetActive;
    public bool activated = false;
    public float speed = 1;

    //Private Members
    //Protected Members

    private void Update(){
        if (fadeOut){
            _FadeOut();
        }
        else if (fadeIn){
            _FadeIn();
        }
    }

    private void _FadeOut() {
        if (canvasGroup.alpha > 0) {
            canvasGroup.alpha -= Time.deltaTime * speed;
        }
        else if (!activated){
            _Activate();
        }
    }

    private void _FadeIn(){
        if (canvasGroup.alpha < 1){
            canvasGroup.alpha += Time.deltaTime * speed;
        }
        else if (!activated) {
            _Activate();
        }
    }

    private void _Activate() {
        if (toSetActive != null) {
            activated = true;
            toSetActive.SetActive(!toSetActive.activeInHierarchy);
        }
    }

    public void In() {
        fadeIn = true;
        fadeOut = false;
    }

    public void Out() {
        fadeIn = false;
        fadeOut = true;
    }
}
