﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MainMenuCursor : PlayerInput{

    //Public Members
    public GameObject[] cursorLocations = new GameObject[3];
    public GameObject cursor;
    public int index = 0;
    public float blinkTimerReset = 1f;
    public float currentBlinkTimer = 1;
    public float inputTimerReset = 0.2f;
    public float currentInputTimer = 0;

    //Private Members
    private string _moveCursor = "";
    private string _submit = "";

    //Added for Credits submenu
    [SerializeField]
    private CamBezier cam;
    [SerializeField]
    private Animator subMenuPanel;
    private bool isMainMenu = true;
    [SerializeField]
    private GameObject mainMenuText;
    [SerializeField]
    private GameObject creditsText;
    [SerializeField]
    private float menuChangeTime;
    private bool canBlink = true;

    //Protected Members

    // Start is called before the first frame update
    void Start(){
        cursor.transform.position = cursorLocations[0].transform.position;
        pDetectOS();

        Debug.Log("Num controllers" + Input.GetJoystickNames().Length);

        if (Input.GetJoystickNames().Length > 0) {
            ChangeToControllerSupport();
        }
        else {
            ChangeToKeyboardSupport();
        }
    }

    // Update is called once per frame
    void Update(){
        pTrackTimers();
        if (canBlink)
            _Blink();
        if (isMainMenu)
        {
            _DetectCursorMovement();
            _UpdateCursorLocation();
        }
        _DetectCursorSelection();
    }


    protected override void pTrackTimers(){
        if (currentInputTimer > 0) {
            currentInputTimer -= Time.deltaTime;
        }

        if (currentBlinkTimer > 0){
            currentBlinkTimer -= Time.deltaTime;
        }
    }

    private void _Blink() { 
        if (currentBlinkTimer <= 0){
            cursor.SetActive(!cursor.activeInHierarchy);
            currentBlinkTimer = blinkTimerReset;
        }
    }

    private void _DetectCursorMovement() {
        if (!pcontrollerPresent) {
            if (Input.GetAxis(_moveCursor) < 0 && currentInputTimer <= 0) {
                index++;
                currentInputTimer = inputTimerReset;
                _MakeCursorVisible();
            }

            if (Input.GetAxis(_moveCursor) > 0 && currentInputTimer <= 0) {
                index--;
                currentInputTimer = inputTimerReset;
                _MakeCursorVisible();
            }
        }
        else{ //The expected axis value will reverse if a controller is present.
            if (Input.GetAxis(_moveCursor) > 0 && currentInputTimer <= 0){
                index++;
                currentInputTimer = inputTimerReset;
                _MakeCursorVisible();
            }

            if (Input.GetAxis(_moveCursor) < 0 && currentInputTimer <= 0){
                index--;
                currentInputTimer = inputTimerReset;
                _MakeCursorVisible();
            }
        }

        //Causes the cursor to loop
        if (index > cursorLocations.Length - 2) {
            index = 0;
        }
        else if (index < 0) {
            index = cursorLocations.Length - 2;
        }
    }

    private void _DetectCursorSelection(){
        if (Input.GetAxis(_submit) > 0 && currentInputTimer <= 0)
        {
            if (index == 0) {
                //Load game
                SceneManager.LoadScene("PlayerScene");
            }
            else if (index == 1) {
                //Open Credits
                canBlink = false;
                isMainMenu = false;
                cam.GoToEnd();
                if (subMenuPanel != null)
                {
                    subMenuPanel.SetBool("isOpen", true);
                }
                StartCoroutine(OpenCredits());
            }
            else if (index == 2) {
                //Quit game
                Application.Quit();
            }
            else if (index == 3)
            {
                //Back to main menu
                canBlink = false;
                cam.GoToBeginning();
                if (subMenuPanel != null)
                {
                    subMenuPanel.SetBool("isOpen", false);
                }
                StartCoroutine(CloseCredits());
            }

            currentInputTimer = inputTimerReset;
        }
    }

    private void _UpdateCursorLocation() {
        cursor.transform.position = cursorLocations[index].transform.position;
    }

    private void _MakeCursorVisible() {
        if (!cursor.activeInHierarchy) {
            cursor.SetActive(true);
            currentBlinkTimer = blinkTimerReset;
        }
    }

    public override void ChangeToKeyboardSupport(){
        Debug.Log("Changed to keyboard support...");
        pcontrollerPresent = false;
        _moveCursor = "VerticalMovement_KB";
        _submit = "Space";
    }

    public override void ChangeToControllerSupport(){
        Debug.Log("Finding controller support...");
        pcontrollerPresent = true;

        if (pOS == "Windows"){
            _moveCursor = "VerticalMovement_XBx_Windows";
            _submit = "A_XBx_Windows";
            Debug.Log("Windows controller support enabled.");
        }
        else if (pOS == "Mac"){
            _moveCursor = "VerticalMovement_XBx_Mac";
            _submit = "A_XBx_Mac";
            Debug.Log("Mac controller support enabled.");
        }
        else if (pOS == "Linux"){
            _moveCursor = "VerticalMovement_XBx_Linux";
            _submit = "A_XBx_Linux";
            Debug.Log("Linux controller support enabled.");
        }
        else{
            Debug.Log("No controller support for this system.");
            ChangeToKeyboardSupport();
        }
    }

    private IEnumerator OpenCredits()
    {
        Debug.Log("Open Credits coroutine");
        mainMenuText.SetActive(false);
        cursor.SetActive(false);
        index = 3;
        cursor.transform.position = cursorLocations[3].transform.position;

        yield return new WaitForSeconds(menuChangeTime);

        creditsText.SetActive(true);
        cursor.SetActive(true);
        canBlink = true;

        Debug.Log("Open Credits coroutine complete");
    }

    private IEnumerator CloseCredits()
    {
        Debug.Log("Close Credits coroutine");
        creditsText.SetActive(false);
        cursor.SetActive(false);
        cursor.transform.position = cursorLocations[0].transform.position;

        yield return new WaitForSeconds(menuChangeTime);

        mainMenuText.SetActive(true);
        index = 0;
        cursor.SetActive(true);
        canBlink = true;
        isMainMenu = true;

        Debug.Log("Close Credits coroutine complete");
    }

}
