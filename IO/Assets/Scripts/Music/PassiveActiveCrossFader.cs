﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassiveActiveCrossFader : MonoBehaviour{

    //Public Members
    public AudioSource passiveMusic;
    public AudioSource activeMusic;
    public EnemyManager enemyManager;
    public Transform playerTransform;

    //Private Members
    [SerializeField]
    private bool _passive = true;

    //Protected Members

    // Start is called before the first frame update
    void Start(){

    }

    // Update is called once per frame
    void Update(){
        if (enemyManager != null) {
            _CheckPassive();
        }
        else {
            _FindEnemyManager();
        }
    }

    private void _CheckPassive() {

        bool inRange = false;

        foreach (Enemy e in enemyManager.enemies) {
            if (e != null) {
                float distance = 0f;
                distance = Vector3.Distance(e.transform.position, transform.position);
                if (distance <= e.agroRange) {
                    inRange = true;
                    break;
                }
            }
        }

        if (inRange) {
            _passive = false;
        }
        else {
            _passive = true;
        }



        if (_passive) {
            _FadeInPassive();
        }
        if (!_passive) {
            _FadeInActive();
        }
    }

    private void _FadeInPassive() {
        if (passiveMusic.volume < 1) {
            passiveMusic.volume += Time.deltaTime;
        }
        if (activeMusic.volume > 0) {
            activeMusic.volume -= Time.deltaTime;
        }
    }

    private void _FadeInActive() {
        if (activeMusic.volume < 1) {
            activeMusic.volume += Time.deltaTime;
        }
        if (passiveMusic.volume > 0) {
            passiveMusic.volume -= Time.deltaTime;
        }
    }

    private void _FindEnemyManager() {
        GameObject enemyManagerObj = GameObject.Find("Enemy Handler");
        if (enemyManagerObj != null) {
            enemyManager = enemyManager.GetComponent<EnemyManager>();
        }
        else {
            Debug.Log("Could not find enemy handler...");
        }
    }

}
